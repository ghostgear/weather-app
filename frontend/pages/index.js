import Footer from "../components/Footer";
import Header from "../components/Header";

import React, { Component } from 'react';
import Chart from "../components/Chart";
import Table from "../components/Table";

class Home extends Component {


    constructor(props) {
        super(props);

        this.state = {
            cityData: []
        }

        this.setCityData = this.setCityData.bind(this);
        this.removeCity = this.removeCity.bind(this);

    }

    setCityData(data) {
        let _cityData = this.state.cityData;
        _cityData.push(data);
        this.setState({
            cityData: _cityData
        });
    }

    removeCity(index) {
        let _cityData = this.state.cityData;

        _cityData.splice(index, 1);
        this.setState({
            cityData: _cityData
        });
    }

    render() {

        const { cityData } = this.state;

        return (
            <div className="d-flex flex-column wrapper">

                <Header cityData={cityData} setCityData={this.setCityData} />

                <div id="page-content">
                    <div className="container">
                        <div className="row">
                            <div className="col-md text-center pt-5">
                                <h1>Compare city temperatures</h1>
                            </div>
                        </div>
                        {
                            cityData.length === 0 ?
                                <div className="row">
                                    <div className="col-md text-center pt-5">
                                        <h4>Just add your first city to compare</h4>
                                    </div>
                                </div> : null
                        }
                        <div className="row">
                            <div className="col-md d-flex justify-content-center">
                                {cityData.length > 0 ? <Chart cityData={cityData.slice()} /> : null}
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md mx-auto">
                                {cityData.length > 0 ? <Table cityData={cityData.slice()} removeCity={this.removeCity} /> : null}
                            </div>
                        </div>
                    </div>

                </div>

                <Footer />


            </div>
        )
    }
}

export default Home;


