import axios from 'axios';

const getByName = async (req, res) => {

    try {
        const { query } = req.query;

        const instance = axios.create({
            baseURL: process.env.APP_FRONTEND_API_URL,
            headers: {
                'Content-Type': 'application/json'
            },
        });;

        await instance.get(`/api/weather/getByName/${query}`)
            .then((res2) => {
                return res.status(200).send(res2.data);
            }).catch((err) => {
                return res
                    .status(err.response.status)
                    .send(err.response.data)
            });;


    } catch (error) {
        return res.status(500).send(error);
    }

};

export default getByName