import React from 'react';
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';


const Chart = (props) => {

    const { cityData } = props;

    return (
        <BarChart
            width={500}
            height={300}
            data={cityData}
            margin={{
                top: 50,
                right: 30,
                left: 30,
                bottom: 50,
            }}
        >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar dataKey="weather.temp" name="Temp on city" fill="rgb(33, 37, 41)" />
        </BarChart>
    );

}

export default Chart;