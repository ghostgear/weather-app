
const Footer = () => {


    return (
        <footer className="flex-shrink-0 py-4">
            <div className="container">
                <div className="row">
                    <div className="col-md text-center">
                        Made by Marcos Silva @ {new Date().getFullYear()} | Project available at: <a href="https://gitlab.com/ghostgear/weather-app">Gitlab</a>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer;
