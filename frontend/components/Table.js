import DataTable from 'react-data-table-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { Confirm } from 'notiflix';
import Image from 'next/image'


const Table = (props) => {

    const { cityData, removeCity } = props;

    const columns = [
        {
            name: 'City',
            selector: row => row.name,
            sortable: true,
            cell: (row, index, column, id) => {
                return (
                    <div className="d-flex align-items-center">
                        <Image
                            src={`https://openweathermap.org/images/flags/${row.country.toLowerCase()}.png`}
                            width={24}
                            height={15}
                            alt="country"
                        />
                        <span className="px-2 align-middle">{row.name}</span>
                    </div>
                )
            }
        },
        {
            name: 'Temperature',
            selector: row => row.weather.temp,
            sortable: true,
            cell: (row, index, column, id) => {
                return (
                    <div className="d-flex align-items-center">
                        <Image
                            src={`http://openweathermap.org/img/wn/${row.weather.icon}@2x.png`}
                            alt="weatherIcon"
                            width={68}
                            height={68}
                        />
                        <span className="px-2 align-middle">{row.weather.temp} ºC</span>
                    </div>
                )
            }
        },
        {
            name: 'Sunrise',
            selector: row => row.dayInfo.sunrise,
            sortable: true,
        },
        {
            name: 'Sunset',
            selector: row => row.dayInfo.sunset,
            sortable: true,
        },
        {
            name: 'Remove city',
            sortable: false,
            center: true,
            cell: (row, index, column, id) => {
                return (
                    <div>
                        <FontAwesomeIcon icon={faTrash} onClick={() => {
                            removeCityFromList(row.id);
                        }} />
                    </div>
                )
            }
        },
    ];

    const removeCityFromList = (id) => {

        let index = -1;
        let obj = cityData.find((e, i) => {
            if (e.id === id) {
                index = i;
                return e;
            }
        });

        Confirm.show(
            'Confirm city delete',
            `Do you want to remove ${obj.name}, ${obj.country}?`,
            'Yes',
            'No',
            () => {
                //afirmative response
                removeCity(index);
            },
            () => {
                //negative response, just dismiss
            },
            {
                width: '320px',
                borderRadius: '8px',
                // etc...
            },
        );
    }

    return (
        <DataTable
            columns={columns}
            data={cityData}
        />
    );
};

export default Table;