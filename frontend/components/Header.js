import { useEffect, useState } from 'react';
import { Notify } from 'notiflix';
import { getWeatherByName } from '../services/weather';


const Header = (props) => {

    const { cityData, setCityData } = props;

    const [cityName, setCityName] = useState("");

    const searchCityClick = (e) => {
        e.preventDefault();

        // Case city name length < 3 don't search
        if (cityName.length < 3) {
            Notify.failure('City name to short', { position: "center-top" });
            return;
        }

        getWeatherByName(cityName).then((res) => {
            let obj = cityData.find((e) => e.id === res.id);
            if (obj !== undefined) {
                Notify.failure(`Duplicated city, choose another city to compare."`, { position: "center-top" });
                return;
            }
            setCityData(res);
            setCityName("");
        }).catch((err) => {
            if (err.response !== undefined && err.response.status === 404) {
                Notify.failure(`City "${cityName}" not found, try another one."`, { position: "center-top" });
                return;
            } else {
                Notify.failure(`Undefined error, please contact our dev team."`, { position: "center-top" });
                return;
            }
        });
    }



    return (
        <nav className="navbar navbar-dark bg-dark">
            <div className="container-fluid">
                <a className="navbar-brand">Weather App</a>
                <form className="d-flex">
                    <input className="form-control me-2" type="search" placeholder="Ex: City, Country" aria-label="Search" value={cityName} onChange={(e) => setCityName(e.target.value)} />
                    <button className="btn btn-outline-success" type="submit" onClick={searchCityClick}>Add City</button>
                </form>
            </div>
        </nav>
    )
}

export default Header;
