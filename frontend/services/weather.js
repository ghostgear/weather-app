import axios from 'axios';

export const getWeatherByName = async (query) => {
    const instance = axios.create();

    const resp = await instance.get(`/api/v1/weather/getByName/${query}`);

    return resp.data;
};