
#weather-app

Small app to compare cities temperatures
Available at: https://weather.marcossilva.eu/



## Deployment

To deploy this project run

- create .env file with the .env.sample
- replace \_\_PLACE_YOUR_OPEN_WEATHER_API_KEY_HERE__ with your Open Weather Api Key

```bash
  docker-compose build
  docker-compose up -d
```


## Features

- Add multiple cities
- Bar Chart with temparature
- Table with temperature, sunrise and sunset sortable


## Authors

- [@ghostgear](https://gitlab.com/ghostgear)

