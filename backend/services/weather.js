import axios from 'axios';
import moment from 'moment';

export const getWeatherByName = async (req, res) => {

    try {
        const { name } = req.params;

        const instance = axios.create({
            baseURL: process.env.OPEN_WEATHER_URL,
            headers: {
                'Content-Type': 'application/json'
            },
        });;

        await instance.get(`/weather`,
            {
                params:
                {
                    q: name,
                    appid: process.env.OPEN_WEATHER_API,
                    units: 'metric'
                }
            }
        ).then((res2) => {

            let toReturnObj = {
                id: res2.data.id,
                name: res2.data.name,
                country: res2.data.sys.country,
                weather: {
                    temp: res2.data.main.temp,
                    icon: res2.data.weather[0].icon,
                    desc: res2.data.weather[0].main
                },
                dayInfo: {
                    sunrise: moment.unix(res2.data.sys.sunrise).format('HH:mm'),
                    sunset: moment.unix(res2.data.sys.sunset).format('HH:mm')
                }
            };

            return res.status(200).send(toReturnObj);
        }).catch((err) => {
            return res
                .status(err.response.status)
                .send(err.response.data)
        });

    } catch (error) {
        console.log(error);
        return res.status(500).send(error);
    }

};
