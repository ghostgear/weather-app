import express from 'express';
import cors from 'cors';
import router from './routes/index.js';
import logger from './middlewares/logger.js';

const server = express();

// Enable cors for all routes
server.use(cors());

// Configuring body request parser 
server.use(express.urlencoded({ extended: false }));
server.use(express.json());

server.use(logger);
server.use(router);

//Start server
server.listen(
    process.env.APP_BACKEND_PORT,
    () => console.log(`Weather server server up on:  http://localhost:${process.env.APP_BACKEND_PORT}!`)
);