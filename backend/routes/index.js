import express from 'express'
import api from './api/index.js'
import base from './base/index.js'

// Express object instance
const router = express()

// Default route
router.use('/', base)
router.use('/api', api)

export default router