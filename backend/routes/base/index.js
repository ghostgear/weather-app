import express from 'express'

const base = express.Router();

base.get('/', (req, res) => res.send('Weather API Server Working'));

export default base;