import express from 'express';
import { getWeatherByName } from '../../services/weather.js';

const api = express.Router();

api.get('/weather/getByName/:name', getWeatherByName);

export default api;