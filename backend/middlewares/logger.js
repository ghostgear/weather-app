import winston from 'winston'
import expressWinston from 'express-winston'

const logger = new expressWinston.logger({
    transports: [
        new winston.transports.File({
            filename: './logs/info.log'
        })
    ],
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json()
    )
});

export default logger